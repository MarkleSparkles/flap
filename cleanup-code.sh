#!/bin/bash
clang-format -style="{BasedOnStyle: llvm, IndentWidth: 4, KeepEmptyLinesAtTheStartOfBlocks: false}" "$@" -i src/*.{c,h}
