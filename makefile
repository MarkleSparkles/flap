CC_GCC := /usr/bin/gcc
CC_CLANG := /usr/bin/clang
RM := /bin/rm

CFLAGS_GCC := -c -std=c11 -Wall -Wextra -pedantic -O3 -Wfloat-equal -ggdb
LFLAGS_GCC := -std=c11 -O3 -ggdb

CSFML := -lcsfml-graphics -lcsfml-system -lcsfml-window 

SRC := $(wildcard src/*.c)
OBJS := $(SRC:.c=.o)

OUT := game

.PHONY : all clean
all: clang

gcc: $(OBJS)
	$(CC_GCC) $(LFLAGS)  $(OBJS) -o $(OUT) $(CSFML) -lm

clang:
	$(CC_CLANG) $(CSFML) $(SRC) -o $(OUT)

%.o: %.c
	$(CC_GCC) $(CFLAGS) -c $< -o $@ -I.

clean:
	$(RM) -f src/*.o game
