#pragma once

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <SFML/Graphics.h>

#include "bird.h"
#include "pipe.h"

/* PipePair structure */
struct PipePair {
    /* Position */
    sfVector2f position;
    /* Velocity */
    sfVector2f velocity;
    /* Child pipes */
    struct Pipe pipe_top;
    struct Pipe pipe_bottom;

    /* Start x saved for restarts */
    float start_x;

    /* whether the pair has been scored */
    bool scored;
};

/* takes starting x position */
void pipepair_load(struct PipePair *, float);

void pipepair_restart(struct PipePair *);

void pipepair_cleanup(struct PipePair *);

void pipepair_update(struct PipePair *, struct Bird *, int *);

void pipepair_draw(struct PipePair *, sfRenderWindow *);

float pipepair_new_y();

void pipepair_set_position(struct PipePair *, float, float);

void pipepair_set_velocity(struct PipePair *, float, float);

void pipepair_change_position(struct PipePair *, sfVector2f *);
