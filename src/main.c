#include "main.h"

/* Window mode (resolution, colour-depth) */
sfVideoMode window_video_mode = {800, 600, 32};
/* Pointer to the window */
sfRenderWindow *window;
/* Variable used to store window events whilst they are processed */
sfEvent window_event;

/* Font */
sfFont *font;

/* Score text */
sfText *text_highscore;
sfText *text_score;

/* Score */
int score = 0;
int highscore = 0;

/* Clock for game updates */
sfClock *loop_clock;
/* Stores whether it is time to update & draw */
bool redraw;

/* Game objects */
struct Background background;
struct Bird bird;
struct PipePair pipepair1;
struct PipePair pipepair2;

/* Entry */
int main() {
    /* Load */
    if (!load()) {
        return EXIT_FAILURE;
    }
    /* Main game loop*/
    loop();

    /* Cleanup */
    cleanup();

    return EXIT_SUCCESS;
}

/* Load everything */
bool load() {
    gamestate = LOADING;

    /* Create the window */
    window = sfRenderWindow_create(window_video_mode, "Flap",
                                   sfResize | sfClose, NULL);
    if (!window) {
        return false;
    }

    /* Load font (Comic Sans ;) */
    font = sfFont_createFromFile("img/comicsans.ttf");
    if (!font) {
        return false;
    }

    /* Create score texts */
    text_score = sfText_create();
    sfText_setFont(text_score, font);
    sfText_setCharacterSize(text_score, 50);

    text_highscore = sfText_create();
    sfText_setFont(text_highscore, font);
    sfText_setCharacterSize(text_highscore, 50);

    /* Move highscore position so that it doesn't overlap */
    sfVector2f position;
    position.x = 0.0f;
    position.y = 50.0f;
    sfText_setPosition(text_highscore, position);

    /* Update the scores */
    update_score();

    /* Load the game object */
    background_load(&background);
    bird_load(&bird);
    pipepair_load(&pipepair1, 864.0f + 928.0f / 2 * 0);
    pipepair_load(&pipepair2, 864.0f + 928.0f / 2 * 1);

    /* Clock used to time the game loop */
    loop_clock = sfClock_create();

    /* Goto menu */
    gamestate = MENU;

    /* Successfully loaded */
    return true;
}

void restart() {
    /* Restart objects */
    bird_restart(&bird);
    pipepair_restart(&pipepair1);
    pipepair_restart(&pipepair2);

    /* Set the highscore if the score is bigger and reset score */
    if (score > highscore) {
        highscore = score;
    }

    score = 0;

    /* Goto menu state*/
    gamestate = MENU;
}

/* Clean up */
void cleanup() {
    /* Cleanup objects */
    background_cleanup(&background);
    bird_cleanup(&bird);
    pipepair_cleanup(&pipepair1);
    pipepair_cleanup(&pipepair2);

    /* Cleanup font and text */
    sfText_destroy(text_score);
    sfText_destroy(text_highscore);
    sfFont_destroy(font);

    /* Cleanup window */
    sfRenderWindow_destroy(window);
}

/* Main game loop*/
void loop() {
    while (sfRenderWindow_isOpen(window)) {
        /* Update everything every 1/FPS of a second */
        if (sfTime_asSeconds(sfClock_getElapsedTime(loop_clock)) >=
            1.0f / FPS) {
            redraw = true;
            sfClock_restart(loop_clock);
        } else {
            /* Sleep until it's time to update again? */
            sfTime sleepTime =
                sfSeconds((1.0f / FPS) -
                          sfTime_asSeconds(sfClock_getElapsedTime(loop_clock)));
            sfSleep(sleepTime);
        }

        /* Process events */
        while (sfRenderWindow_pollEvent(window, &window_event)) {
            /* Close window : exit */
            if (window_event.type == sfEvtClosed)
                sfRenderWindow_close(window);
        }

        /* Update and redraw if it is time to */
        if (redraw) {
            update();
            draw();
            redraw = false;
        }
    }
}

void update() {
    /* Update menu */
    if (gamestate == MENU) {
        if (sfKeyboard_isKeyPressed(sfKeySpace)) {
            gamestate = GAME;
        }
        /* Update Game */
    } else if (gamestate == GAME) {
        bird_update(&bird);
        pipepair_update(&pipepair1, &bird, &score);
        pipepair_update(&pipepair2, &bird, &score);

        /* If the bird has fallen, restart */
        if (bird.position.y > 1000.0f) {
            restart();
        }
    } else {
        printf("%i%s\n", gamestate, " -> UNKNOWN GAMSTATE!");
    }

    /* Update the score text */
    update_score();
}

/* Update the score texts */
void update_score() {
    char string_score[20];
    sprintf(string_score, "%s%i", "Score: ", score);
    sfText_setString(text_score, string_score);

    char string_highscore[20];
    sprintf(string_highscore, "%s%i", "Highscore: ", highscore);
    sfText_setString(text_highscore, string_highscore);
}

void draw() {
    /* Clear the screen */
    sfRenderWindow_clear(window, sfBlack);

    /* Draw background */
    background_draw(&background, window);

    /* Draw pipes */
    pipepair_draw(&pipepair1, window);
    pipepair_draw(&pipepair2, window);

    /* Draw bird */
    bird_draw(&bird, window);

    /* Draw score */
    sfRenderWindow_drawText(window, text_score, NULL);
    sfRenderWindow_drawText(window, text_highscore, NULL);

    /* Update the window */
    sfRenderWindow_display(window);
}
