#pragma once

#include <stdio.h>

#include <SFML/Graphics.h>

struct Background {
    /* Position */
    sfVector2f position;
    /* Texture and Sprite for the bird */
    sfTexture *texture;
    sfSprite *sprite;
};

void background_load(struct Background *background);

void background_cleanup(struct Background *background);

void background_draw(struct Background *background, sfRenderWindow *window);

void background_set_position(struct Background *, float x, float y);