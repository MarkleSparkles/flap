#include "bird.h"

/* Load the bird */
void bird_load(struct Bird *bird) {
    /* Set initial position, origin and velocity */
    bird_set_position(bird, 400.0f, 300.0f);
    bird_set_origin(bird, 32.0f, 32.0f);
    bird_set_velocity(bird, 0.0f, 0.0f);

    /* Set the radius and rotation */
    bird->radius = BIRD_RADIUS;
    bird->rotation = 0.0f;

    /* Load the texture */
    bird->texture = sfTexture_createFromFile("img/bird.png", NULL);
    if (!bird->texture)
        printf("Failed to load bird texture!");
    bird->sprite = sfSprite_create();

    /* Setup the bird's SFML sprite */
    sfSprite_setTexture(bird->sprite, bird->texture, sfTrue);
    sfSprite_setPosition(bird->sprite, bird->position);
    sfSprite_setOrigin(bird->sprite, bird->origin);
}

/* Reset the bird */
void bird_restart(struct Bird *bird) {
    /* Set reset position and velocity */
    bird_set_position(bird, 400.0f, 300.0f);
    bird_set_velocity(bird, 0.0f, 0.0f);
    /* Set bird position */
    sfSprite_setPosition(bird->sprite, bird->position);

    /* Reset rotation */
    bird->rotation = 0.0f;
    /* Set sprite position */
    sfSprite_setRotation(bird->sprite, bird->rotation);

    /* Reset death hop */
    bird->hop = false;

    /* Set bird to be alive */
    bird->dead = false;
}

/* Cleanup bird */
void bird_cleanup(struct Bird *bird) {
    sfSprite_destroy(bird->sprite);
    sfTexture_destroy(bird->texture);
}

/* Update the bird */
void bird_update(struct Bird *bird) {
    if (bird->dead) {
        /* If the bird is 'dead', bounce and fall */
        /* Hop if it hasn't already */
        if (!bird->hop) {
            /* Send the bird to bounce away */
            bird_set_velocity(bird, -8.0f, BIRD_JUMP_VELOCITY);
            bird->rotation = 0.0f;
            bird->hop = true;
        }
        /* Sends the bird spinnig */
        bird->rotation += -8.0f;
        /* Add 'gravity' */
        bird_change_velocity(bird, 0.0f, BIRD_FALL_ACCELERATION);
    } else {
        /* Otherwise, update as normal */
        /* Bounce when Space is down */
        if (sfKeyboard_isKeyPressed(sfKeySpace)) {
            bird_set_velocity(bird, 0.0f, BIRD_JUMP_VELOCITY);
        }

        /* Add 'gravity' */
        bird_change_velocity(bird, 0.0f, BIRD_FALL_ACCELERATION);
        /* Update the bird's rotation to reflect it's y velocity */
        bird->rotation = bird->velocity.y * 2.0f;
    }

    /* Update the bird's position */
    bird_change_position(bird, &bird->velocity);

    /* Update the bird's sprite */
    sfSprite_setPosition(bird->sprite, bird->position);
    sfSprite_setRotation(bird->sprite, bird->rotation);
}

/* Draw the bird */
void bird_draw(struct Bird *bird, sfRenderWindow *window) {
    sfRenderWindow_drawSprite(window, bird->sprite, NULL);
}

/* Set the bird's position to the given floats*/
void bird_set_position(struct Bird *bird, float x, float y) {
    bird->position.x = x;
    bird->position.y = y;
}

/* Set the bird's origin to the given floats*/
void bird_set_origin(struct Bird *bird, float x, float y) {
    bird->origin.x = x;
    bird->origin.y = y;
}

/* Set the bird's velocity to the given floats*/
void bird_set_velocity(struct Bird *bird, float x, float y) {
    bird->velocity.x = x;
    bird->velocity.y = y;
}

/* Change the bird's position by the given vector */
void bird_change_position(struct Bird *bird, sfVector2f *vector) {
    bird->position.x += vector->x;
    bird->position.y += vector->y;
}

/* Change the bird's velocity by the given floats */
void bird_change_velocity(struct Bird *bird, float x, float y) {
    bird->velocity.x += x;
    bird->velocity.y += y;
}
