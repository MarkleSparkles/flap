#pragma once

#include <stdbool.h>

#include <SFML/Graphics.h>
#include <SFML/System.h>
#include <SFML/Window.h>

#include "background.h"
#include "bird.h"
#include "pipe.h"
#include "pipepair.h"

/* Constants for program exit codes */
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

/* Our target Frames Per Second (FPS) */
#define FPS 60.0f

/* Game state */
#define LOADING 1
#define MENU 2
#define GAME 3

int gamestate;

bool load();

void restart();

void cleanup();

void loop();

void update();

void update_score();

void draw();
