#pragma once

#include <stdbool.h>
#include <stdio.h>

#include <SFML/Graphics.h>

/* The velocity of the bird's jump */
#define BIRD_JUMP_VELOCITY -10.0f
/* The acceleration of gravity/fall */
#define BIRD_FALL_ACCELERATION 0.5f
/* The radius of the bird's image file */
#define BIRD_RADIUS 62.0f // should be 64.0f but extra room for collisions

/* Bird Structure */
struct Bird {
    /* Position, origin, velocity and rotation for the bird */
    sfVector2f position;
    sfVector2f velocity;
    sfVector2f origin;
    float radius;
    float rotation;
    bool dead;
    bool hop;
    /* Texture and Sprite for the bird */
    sfTexture *texture;
    sfSprite *sprite;
};

void bird_load(struct Bird *);

void bird_restart(struct Bird *);

void bird_cleanup(struct Bird *);

void bird_update(struct Bird *);

void bird_draw(struct Bird *, sfRenderWindow *);

/* Setters & modifiers */

void bird_set_position(struct Bird *, float, float);

void bird_set_velocity(struct Bird *, float, float);

void bird_set_origin(struct Bird *, float, float);

void bird_change_position(struct Bird *, sfVector2f *);

void bird_change_velocity(struct Bird *, float, float);
