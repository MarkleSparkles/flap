#pragma once

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <SFML/Graphics.h>

#include "bird.h"

#define PIPE_WIDTH 128.0f
#define PIPE_HEIGHT 512.0f
#define PIPE_GAP 250.0f
#define PIPE_GAPI 250

/* Pipe Structure */
struct Pipe {
    /* Position, origin and rotation for the bird */
    sfVector2f size;
    sfVector2f position;
    sfVector2f offset;
    sfVector2f origin;
    float rotation;
    /* Texture and Sprite for the bird */
    sfTexture *texture;
    sfSprite *sprite;
};

void pipe_load(struct Pipe *, bool, float, float);

void pipe_restart(struct Pipe *, float, float);

void pipe_cleanup(struct Pipe *);

void pipe_update(struct Pipe *, sfVector2f *, struct Bird *);

void pipe_draw(struct Pipe *, sfRenderWindow *);

bool pipe_bird_intersects(struct Pipe *, struct Bird *);

float sqr(float f);

void pipe_set_position(struct Pipe *, float, float);

void pipe_set_origin(struct Pipe *, float, float);

void pipe_set_rotation(struct Pipe *, float);

void pipe_set_size(struct Pipe *, float, float);

void pipe_set_offset(struct Pipe *, float, float);
