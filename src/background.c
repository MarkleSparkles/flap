#include "background.h"

/* Load the background */
void background_load(struct Background *background) {
    /* Set position */
    background_set_position(background, 0.0f, 0.0f);

    /* Load texture */
    background->texture = sfTexture_createFromFile("img/background.png", NULL);
    if (!background->texture) {
        printf("Failed to load background!");
    }

    /* Setup sprite */
    background->sprite = sfSprite_create();
    sfSprite_setTexture(background->sprite, background->texture, sfTrue);
    sfSprite_setPosition(background->sprite, background->position);
}

void background_cleanup(struct Background *background) {
    sfSprite_destroy(background->sprite);
    sfTexture_destroy(background->texture);
}

/* Draw the background */
void background_draw(struct Background *background, sfRenderWindow *window) {
    /* Draw the sprite */
    sfRenderWindow_drawSprite(window, background->sprite, NULL);
}

/* Set the position to the given floats */
void background_set_position(struct Background *background, float x, float y) {
    background->position.x = x;
    background->position.y = y;
}
