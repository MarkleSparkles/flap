#include "pipepair.h"

void pipepair_load(struct PipePair *pipepair, float start_x) {
    /* Set position & velocity*/
    pipepair_set_position(pipepair, start_x, pipepair_new_y());
    pipepair_set_velocity(pipepair, -8.0f, 0.0f);

    /* save start x for a restart */
    pipepair->start_x = start_x;

    /* Set the gap between top and bottom pipes */
    float pipe_gap = 200.0f;

    /* Load the child pipes */
    pipe_load(&pipepair->pipe_top, true, pipepair->position.x,
              pipepair->position.y);
    pipe_load(&pipepair->pipe_bottom, false, pipepair->position.x,
              pipepair->position.y);
}

void pipepair_restart(struct PipePair *pipepair) {
    pipepair_set_position(pipepair, pipepair->start_x, pipepair_new_y());
    pipepair_set_velocity(pipepair, -8.0f, 0.0f);

    pipe_restart(&pipepair->pipe_top, pipepair->position.x,
                 pipepair->position.y);
    pipe_restart(&pipepair->pipe_bottom, pipepair->position.x,
                 pipepair->position.y);

    pipepair->scored = false;
}

void pipepair_cleanup(struct PipePair *pipepair) {
    pipe_cleanup(&pipepair->pipe_top);
    pipe_cleanup(&pipepair->pipe_bottom);
}

void pipepair_update(struct PipePair *pipepair, struct Bird *bird, int *score) {
    /* If the pipe has moved past the left of the screen, move it over to the
     * right */
    if (pipepair->position.x < 0.0f - pipepair->pipe_top.size.x / 2) {
        pipepair_set_position(pipepair, pipepair->position.x + 800.0f +
                                            pipepair->pipe_top.size.x,
                              pipepair_new_y());
        pipepair->scored = false;
    }

    if (pipepair->position.x < 400.0f && !pipepair->scored) {
        *score = *score + 1;
        pipepair->scored = true;
    }
    /* Update the position */
    pipepair_change_position(pipepair, &pipepair->velocity);

    /* Update child pipes */
    pipe_update(&pipepair->pipe_top, &pipepair->position, bird);
    pipe_update(&pipepair->pipe_bottom, &pipepair->position, bird);

    /* Stop moving if the bird is dead */
    if (bird->dead) {
        pipepair_set_velocity(pipepair, 0.0f, 0.0f);
    }
}

void pipepair_draw(struct PipePair *pipepair, sfRenderWindow *window) {
    /* Draw child pipes */
    pipe_draw(&pipepair->pipe_top, window);
    pipe_draw(&pipepair->pipe_bottom, window);
}

float pipepair_new_y() {
    return PIPE_GAP / 2 + rand() % (600 - PIPE_GAPI); // 300.0f;
}

void pipepair_set_position(struct PipePair *pipepair, float x, float y) {
    pipepair->position.x = x;
    pipepair->position.y = y;
}

void pipepair_set_velocity(struct PipePair *pipepair, float x, float y) {
    pipepair->velocity.x = x;
    pipepair->velocity.y = y;
}

void pipepair_change_position(struct PipePair *pipepair, sfVector2f *vector) {
    pipepair->position.x += vector->x;
    pipepair->position.y += vector->y;
}
