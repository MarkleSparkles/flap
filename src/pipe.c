#include "pipe.h"

/* Load the pipe */
void pipe_load(struct Pipe *pipe, bool flipped, float start_x, float start_y) {
    /* Set position, size and origin */
    pipe_set_position(pipe, start_x, start_y);
    pipe_set_origin(pipe, PIPE_WIDTH / 2, PIPE_HEIGHT / 2);
    pipe_set_size(pipe, PIPE_WIDTH, PIPE_HEIGHT);

    /* If the pipe if the top pipe, turn the sprite around 180 */
    if (flipped /* top */) {
        pipe_set_offset(pipe, 0.0f, -(pipe->size.y / 2 + PIPE_GAP / 2));
        pipe_set_rotation(pipe, 180.0f);
    } else /* bottom */ {
        pipe_set_offset(pipe, 0.0f, (pipe->size.y / 2 + PIPE_GAP / 2));
        pipe_set_rotation(pipe, 0.0f);
    }

    /* Load the texture (warning, this loads the sprite for EACH INSTANCE OF A
     * PIPE!! (not good)) */
    pipe->texture = sfTexture_createFromFile("img/pipe.png", NULL);
    if (!pipe->texture)
        printf("Failed to load pipe texture!");

    /* Create the sprite */
    pipe->sprite = sfSprite_create();
    sfSprite_setTexture(pipe->sprite, pipe->texture, sfTrue);
    sfSprite_setOrigin(pipe->sprite, pipe->origin);
    sfSprite_setPosition(pipe->sprite, pipe->position);
}

/* Restart the pipe's position */
void pipe_restart(struct Pipe *pipe, float start_x, float start_y) {
    /* Restart position */
    pipe_set_position(pipe, start_x, start_y);
    /* Set sprite position */
    sfSprite_setPosition(pipe->sprite, pipe->position);
}

void pipe_cleanup(struct Pipe *pipe) {
    sfSprite_destroy(pipe->sprite);
    sfTexture_destroy(pipe->texture);
}

/* Update pipe */
void pipe_update(struct Pipe *pipe, sfVector2f *parent_position,
                 struct Bird *bird) {
    /* Update position */
    pipe_set_position(pipe, parent_position->x + pipe->offset.x,
                      parent_position->y + pipe->offset.y);

    /* Update sprite position */
    sfSprite_setPosition(pipe->sprite, pipe->position);

    /* If the bird is touching the pipe, kill it */
    if (!bird->dead && pipe_bird_intersects(pipe, bird)) {
        bird->dead = true;
    }
}

/* Draw pipe */
void pipe_draw(struct Pipe *pipe, sfRenderWindow *window) {
    sfSprite_setRotation(pipe->sprite, pipe->rotation);
    sfRenderWindow_drawSprite(window, pipe->sprite, NULL);
}

/* Check for collision between bird and pipe */
bool pipe_bird_intersects(struct Pipe *pipe, struct Bird *bird) {
    /* Distance from center to center of bird and pipe on each axis*/
    float distanceX = fabsf(pipe->position.x - bird->position.x);
    float distanceY = fabsf(pipe->position.y - bird->position.y);

    /* Return false if the bird is definitely not close enough to collide
     * (without needing to check corner) */
    if (distanceX > (pipe->size.x / 2.0f + bird->radius / 2)) {
        return false;
    }
    if (distanceY > (pipe->size.y / 2.0f + bird->radius / 2)) {
        return false;
    }

    /* Return true if the bird is definitely close enough to collide (without
     * needing to check corner) */
    if (distanceX <= (pipe->size.x / 2.0f)) {
        return true;
    }
    if (distanceY <= (pipe->size.y / 2.0f)) {
        return true;
    }

    /* Calculate the corner distance */
    /* Note: Not square-rooting to save performance, instead the radius will be
     * square to match */
    float cornerDistance = sqr(distanceX - pipe->size.x / 2.0f) +
                           sqr(distanceY - pipe->size.y / 2.0f);

    /* Return whether there is a corner collision or not */
    return (cornerDistance <= sqr(bird->radius));
}

/* Square the given float */
float sqr(float f) { return f * f; }

/* Set position to the given floats */
void pipe_set_position(struct Pipe *pipe, float x, float y) {
    pipe->position.x = x;
    pipe->position.y = y;
}

/* Set origin to given floats */
void pipe_set_origin(struct Pipe *pipe, float x, float y) {
    pipe->origin.x = x;
    pipe->origin.y = y;
}

/* Set roation to given float */
void pipe_set_rotation(struct Pipe *pipe, float rotation) {
    pipe->rotation = rotation;
}

/* Set size to the given floats */
void pipe_set_size(struct Pipe *pipe, float x, float y) {
    pipe->size.x = x;
    pipe->size.y = y;
}

/* Set offset to the given floats */
void pipe_set_offset(struct Pipe *pipe, float x, float y) {
    pipe->offset.x = x;
    pipe->offset.y = y;
}
